#Initial loading of packages and libraries
using LinearAlgebra
using Random
using Pkg
using Parameters
using Random
using StatsBase
using Distributions
using Plots
using LaTeXStrings
using JLD
using Debugger

#If ticketsOn == 0, use strataLib_2_Backup and first params, with outerR = 4.0, Otherwise use strataLib_2 and second params

include("odeLib_MS_system.jl")
#include("strataLib_2.jl") 
include("strataLib_2_Backup.jl") 

#params=strataParams(ode=LangevinParams(h=0.0025, beta=10, epsilon=0.1), innerR = 1.0, outerR =4.0)
params=strataParams(ode=LangevinParams(h=0.0025, beta=10, epsilon=0.1))

#Load the pre-generated estimate of the true x_1 histogram
bin_endpoints = -2.2:0.022:2.2
bin_endpoints_inj = -1.0:0.001:1.0
hist_true = load("true_hist_MS_estimate.jld", "hist_true")

hist_true_occ = load("true_occ_meas_and_weights_3_reg.jld", "hist_true_occ")
weights_true = load("true_occ_meas_and_weights_3_reg.jld", "weights_true")


#Five-strata version, tilted and non-tilted
#Define the strata and initialize the local systems
#J=5
#a = 0.15
#b = 10.5
#a = 1.0 / 6.35
#b = 1.0 / 0.404
#c = 1.0 / 1.39
#Q1 = [1.0/a 1.0/c; 1.0/c 1.0/b]
#Q1 = [1.0/a 0.0; 0.0 1.0/b]

#Q_list = [Q1, Q1, Q1, Q1, Q1]

#center_points = [ODEState([0.0,-0.0]), ODEState([0.65,0.0]), ODEState([-0.65, 0.0]), ODEState([1.3,-0.0]), ODEState([-1.3,-0.0])]

#9 Vertical strata, smaller inside strata
#J=9
#a = 0.05
#b = 10.5
#Q1 = [1.0/a 0.0; 0.0 1.0/b]

#a = 0.15
#b = 10.5
#Q2 = [1.0/a 0.0; 0.0 1.0/b]

#Q_list = [Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1]

#center_points = [ODEState([0.0,0.0]), ODEState([0.35,0.0]), ODEState([-0.35, 0.0]), ODEState([0.7,0.0]), ODEState([-0.7,0.0]), ODEState([1.05,0.0]), ODEState([-1.05,0.0]),  ODEState([1.4,0.0]),  ODEState([-1.4,0.0])]

#11 Vertical strata
#J=11
#a = 0.040
#b = 10.5
#Q1 = [1.0/a 0.0; 0.0 1.0/b]

#Q_list = [Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1, Q1]

#center_points = [ODEState([0.0,0.0]), ODEState([0.25,0.0]), ODEState([-0.25, 0.0]), ODEState([0.5,0.0]), ODEState([-0.5,0.0]), ODEState([0.75,0.0]), ODEState([-0.75,0.0]),  ODEState([1.0,0.0]),  ODEState([-1.0,0.0]), ODEState([1.25,0.0]),  ODEState([-1.25,0.0])]


#7 Vertical strata, smaller inside strata
#J=7
#a = 0.05
#b = 10.5
#Q1 = [1.0/a 0.0; 0.0 1.0/b]

#a = 0.15
#b = 10.5
#Q2 = [1.0/a 0.0; 0.0 1.0/b]

#Q_list = [Q1, Q1, Q1, Q1, Q1, Q2, Q2]

#center_points = [ODEState([0.0,0.0]), ODEState([0.4,0.0]), ODEState([-0.4, 0.0]), ODEState([0.8,0.0]), ODEState([-0.8,0.0]), ODEState([1.4,0.0]), ODEState([-1.4,0.0])]

#Circular strata version
#J=6
#a = 0.7
#b = 0.7
#Q1 = [1.0/a 0.0; 0.0 1.0/b]
#a = 0.35
#b = 0.35
#Q2 = [1.0/a 0.0; 0.0 1.0/b]

#Q_list = [Q1, Q1, Q1, Q1, Q2, Q2]

#center_points = [ODEState([0.4,0.6]), ODEState([0.4,-0.6]), ODEState([-0.4,-0.6]), ODEState([-0.4, 0.6]), ODEState([1.1, 0.0]), ODEState([-1.1, 0.0])]


#3 Vertical Strata
J=3
a = 0.45
b = 10.5
Q1 = [1.0 / a 0.0; 0.0 1.0 / b]

Q_list = [Q1, Q1, Q1]
center_points = [ODEState([0.0,-0.0]), ODEState([1.1,0.0]), ODEState([-1.1, 0.0])]






center_points_new = deepcopy(center_points)
Q_list_new = deepcopy(Q_list)

strataList = AbstractStrata[]
ls_list = LocalSystem[]    
#for i in 1:size(center_points)[1]
#    Q = Q_list[i]
#    p1 = center_points[i]
#    p11 = deepcopy(p1)
#    strata1=ellipticalStrata_Rn(p11,Q)
#    registerStrata!(strataList,strata1,params)
#    ls1=LocalSystem(strata1,p1,PointDistibution,PointDistibutionWeights,params, -1.0)
#    push!(ls_list, ls1)
#end
ls_list = create_2d_strata_list(center_points_new, Q_list_new, -1.0, params)

#Set the parameters for the algorithm runs
n_exits_single_run = 1000
n_iter_single_run = 40
n_burn_in_single_run = 20
max_points_single_run = 10000

n_exits_range = [100]
n_iter_error_run = [5]
n_burn_in_error_run = 1
max_points_error_run = 10000
n_runs_error = 1

ticketsOn = 0







#Code that does not change begins here (i.e. code that won't be renamed for each run)
#############################
#############################



#Run the algorithm to generate error plots
println("Starting Error Plot Run")
err_vals_compare = Any[]
err_occ_vals_compare = Any[]
for i in 1:J
    push!(err_occ_vals_compare, Any[])
end
err_weights_compare = Any[]
time_vals_compare = Any[]
hist_compare = Any[]
flux_compare = Any[]
flux_weights_compare = Any[]

for i in 1:size(n_exits_range)[1]
    local center_points_new = deepcopy(center_points)
    local Q_list_new = deepcopy(Q_list)
    local ls_list = create_2d_strata_list(center_points_new, Q_list_new, -1.0, params)
    n_exits = n_exits_range[i]
    run_length = n_iter_error_run[i]
    
    if ticketsOn == 1
        local_dist_data = stratified_iterations_constant_exits_memory_p_error_inj_flux(ls_list, n_exits, run_length, n_burn_in_error_run, 0.5, max_points_error_run, bin_endpoints, bin_endpoints_inj, hist_true, hist_true_occ, weights_true, ticketsOn, params)
    end
    if ticketsOn == 0
        local_dist_data = stratified_iterations_constant_exits_memory_p_error_inj_flux(ls_list, n_exits, run_length, n_burn_in_error_run, 0.5, max_points_error_run, bin_endpoints, bin_endpoints_inj, hist_true, hist_true_occ, weights_true, params)
    end
    err_vals = local_dist_data[3]
    time_vals = local_dist_data[2]
    time_vals_sum = cumsum(time_vals)
    hist_vals = local_dist_data[1]
    flux_vals = local_dist_data[4]
    flux_weights_vals = local_dist_data[5]
    err_occ_vals = local_dist_data[6]
    err_weights_vals = local_dist_data[7]
    
    n_runs = n_runs_error
    for i in 1:(n_runs-1)
        println(n_exits)
        println(i + 1)
        
        center_points_new = deepcopy(center_points)
        Q_list_new = deepcopy(Q_list)
        ls_list = create_2d_strata_list(center_points_new, Q_list_new, -1.0, params)
        if ticketsOn == 1
            local_dist_data = stratified_iterations_constant_exits_memory_p_error_inj_flux(ls_list, n_exits, run_length, n_burn_in_error_run, 0.5, max_points_error_run, bin_endpoints, bin_endpoints_inj, hist_true, hist_true_occ, weights_true, ticketsOn, params)
        end
        if ticketsOn == 0
            local_dist_data = stratified_iterations_constant_exits_memory_p_error_inj_flux(ls_list, n_exits, run_length, n_burn_in_error_run, 0.5, max_points_error_run, bin_endpoints, bin_endpoints_inj, hist_true, hist_true_occ, weights_true, params)
        end

        err_vals += local_dist_data[3]
        time_vals = local_dist_data[2]
        time_vals_sum += cumsum(time_vals)
        hist_vals += local_dist_data[1]
        flux_vals += local_dist_data[4]
        flux_weights_vals += local_dist_data[5]
        for i in 1:J
            err_occ_vals[i] += local_dist_data[6][i]
        end
        err_weights_vals += local_dist_data[7]
    end
    
    err_vals = err_vals ./ n_runs
    time_vals_sum = time_vals_sum ./ n_runs
    hist_vals = hist_vals ./ n_runs
    flux_vals = flux_vals ./ n_runs
    flux_weights_vals = flux_weights_vals ./ n_runs
    for i in 1:J
        err_occ_vals[i] = err_occ_vals[i] ./ n_runs
    end
    err_weights_vals = err_weights_vals ./ n_runs
    
    push!(err_vals_compare, err_vals)
    push!(time_vals_compare, time_vals_sum)
    push!(hist_compare, hist_vals) 
    push!(flux_compare, flux_vals)
    push!(flux_weights_compare, flux_weights_vals)
    for i in 1:J
        push!(err_occ_vals_compare[i], err_occ_vals[i])
    end
    push!(err_weights_compare, err_weights_vals)
end

plot(time_vals_compare[1], err_vals_compare[1], title = "Error in x over time", label = string(string(n_exits_range[1]), " Exits"))
for i in 1:(size(n_exits_range)[1]-2)
    plot!(time_vals_compare[i+1], err_vals_compare[i+1], title = "Error in x over time", label = string(string(n_exits_range[i+1]), " Exits"))
end
Plot_error_time = plot!(time_vals_compare[size(n_exits_range)[1]], err_vals_compare[size(n_exits_range)[1]], title = "Error in x over time", label = string(string(n_exits_range[size(n_exits_range)[1]]), " Exits"))

plot([log(time_vals_compare[1][i]) for i in 1:n_iter_error_run[1]-n_burn_in_error_run], [log(err_vals_compare[1][i]) for i in 1:n_iter_error_run[1]-n_burn_in_error_run], title = "Log Error in x over time", label = string(string(n_exits_range[1]), " Exits"))
for k in 1:(size(n_exits_range)[1]-2)
    plot!([log(time_vals_compare[k+1][i]) for i in 1:n_iter_error_run[k+1]-n_burn_in_error_run], [log(err_vals_compare[k+1][i]) for i in 1:n_iter_error_run[k+1]-n_burn_in_error_run], title = "Log Error in x over time", label = string(string(n_exits_range[k+1]), " Exits"))
end
Plot_log_error_time =  plot!([log(time_vals_compare[size(n_exits_range)[1]][i]) for i in 1:n_iter_error_run[size(n_exits_range)[1]]-n_burn_in_error_run], [log(err_vals_compare[size(n_exits_range)[1]][i]) for i in 1:n_iter_error_run[size(n_exits_range)[1]]-n_burn_in_error_run], title = "Log Error in x over time", label = string(string(n_exits_range[size(n_exits_range)[1]]), " Exits"))

error_hist_plot = plot(bin_endpoints[1:size(bin_endpoints)[1]-1], hist_compare[size(n_exits_range)[1]], title = "Histogram of First Coordinate, Stratified Maier-Stein", label = string(string(n_exits_range[size(n_exits_range)[1]]), " Exits"))

plot(time_vals_compare[1], flux_compare[1], title = "Fluctuation in injection measure over time", label = string(string(n_exits_range[1]), " Exits"))
for i in 1:(size(n_exits_range)[1]-2)
    plot!(time_vals_compare[i+1], flux_compare[i+1], title = "Fluctuation in injection measure over time", label = string(string(n_exits_range[i+1]), " Exits"))
end
Plot_flux_time = plot!(time_vals_compare[size(n_exits_range)[1]], flux_compare[size(n_exits_range)[1]], title = "Fluctuation in injection measure over time", label = string(string(n_exits_range[size(n_exits_range)[1]]), " Exits"))

plot(time_vals_compare[1], flux_weights_compare[1], title = "Fluctuation in injection weights over time", label = string(string(n_exits_range[1]), " Exits"))
for i in 1:(size(n_exits_range)[1]-2)
    plot!(time_vals_compare[i+1], flux_weights_compare[i+1], title = "Fluctuation in injection weights over time", label = string(string(n_exits_range[i+1]), " Exits"))
end
Plot_flux_weights_time = plot!(time_vals_compare[size(n_exits_range)[1]], flux_weights_compare[size(n_exits_range)[1]], title = "Fluctuation in injection weights over time", label = string(string(n_exits_range[size(n_exits_range)[1]]), " Exits"))





#Run the algorithm to generate a 2d histogram
println("Starting Memory Run")
center_points_new = deepcopy(center_points)
Q_list_new = deepcopy(Q_list)
ls_list = create_2d_strata_list(center_points_new, Q_list_new, -1.0, params)

if ticketsOn == 1
    local_dist_data = stratified_iterations_constant_exits_memory(ls_list, n_exits_single_run, n_iter_single_run, n_burn_in_single_run, 0.5, max_points_single_run, ticketsOn, params)
end
if ticketsOn == 0
    local_dist_data = stratified_iterations_constant_exits_memory(ls_list, n_exits_single_run, n_iter_single_run, n_burn_in_single_run, 0.5, max_points_single_run, params)
end

local_point_dists = local_dist_data[1]
local_weights = local_dist_data[2]
langevin_dist_tot_data = create_total_distribution_2D_langevin(local_point_dists, local_weights, J)
p_dist_tot = langevin_dist_tot_data[1]
q_dist_tot = langevin_dist_tot_data[2]
q_dist_tot_weights = langevin_dist_tot_data[3]

for i in 1:J
    for k in 1:n_exits_single_run
        tk = rand()
        if ticketsOn == 1
            ls_list[i].ticket = tk
        end
        if ticketsOn == 0
            ls_list[i].ticket = -1.0
        end
        evolveLocalSystemToExit!(ls_list[i],ls_list,params,params.ode.h)
    end
end
new_weights_data = find_new_G_weights_eig(ls_list)
new_weights = new_weights_data[2]

Plot_2d_hist = histogram2d(p_dist_tot, q_dist_tot, weights=q_dist_tot_weights ./ ((0.022^2) * sum(q_dist_tot_weights)), bins=1000)

#Find the normalizing constants for the injection distributions
Z_inj = 0.0
for i in 1:J
    global Z_inj += new_weights[i] * size(ls_list[i].injectionDist.points)[1]
end
Z_inj = Z_inj * (0.022^2)

ls_current = ls_list[1]
histogram2d([p.x[1] for p in ls_current.injectionDist.points], [p.x[2] for p in ls_current.injectionDist.points], weights = [new_weights[1] / (Z_inj) for p in ls_current.injectionDist.points], bins=100)
for i in 1:(size(ls_list)[1]-2)
    local ls_current = ls_list[i+1]
    histogram2d!([p.x[1] for p in ls_current.injectionDist.points], [p.x[2] for p in ls_current.injectionDist.points], weights = [new_weights[i+1] / (Z_inj) for p in ls_current.injectionDist.points], bins=100)
end
ls_current = ls_list[size(ls_list)[1]]
Plot_inj_meas = histogram2d!([p.x[1] for p in ls_current.injectionDist.points], [p.x[2] for p in ls_current.injectionDist.points], weights = [new_weights[size(ls_list)[1]] / (Z_inj) for p in ls_current.injectionDist.points], bins=100)

j = 1
plot(1:size(local_dist_data[4][j])[1],local_dist_data[4][j], title = "Number of Injection Measure Points over Time", label = string("Stratum ", string(j)))
for i in 1:(size(ls_list)[1]-2)
    local j = i+1
    plot!(1:size(local_dist_data[4][j])[1],local_dist_data[4][j], title = "Number of Injection Measure Points over Time", label = string("Stratum ", string(j)))
end
j = size(ls_list)[1]
Plot_inj_sizes = plot!(1:size(local_dist_data[4][j])[1],local_dist_data[4][j], title = "Number of Injection Measure Points over Time", label = string("Stratum ", string(j)))



#############################
#############################
#Un-changing code ends here. Code below must be given a name appropriate for the current run of the algorithm





#save the important variables
#save("7_19_4_0_ticket_avged_exits_flux_compare_7_vert_strata_MS.jld", "err_vals_compare", err_vals_compare, "time_vals_compare", time_vals_compare, "hist_compare", hist_compare, "flux_compare", flux_compare, "n_exits_range", n_exits_range, "flux_weights_compare", flux_weights_compare, "err_occ_vals_compare", err_occ_vals_compare, "err_weights_compare", err_weights_compare)

#Save plots
#savefig(Plot_2d_hist, "7_19_4_0_ticket_MS_avged_exits_flux_7_vert_strata_2d_hist.png")
savefig(Plot_2d_hist, "7_20_density_no_ticket_MS_avged_exits_flux_3_reg_strata_2d_hist.png")
#savefig(Plot_inj_meas, "7_19_4_0_ticket_density_inj_meas_7_vert_strata_ticket_inj_meas.png")
#savefig(Plot_error_time, "7_19_4_0_ticket_MS_avged_exits_flux_7_vert_strata_error_plot.png")
#savefig(Plot_log_error_time, "7_19_4_0_ticket_MS_avged_exits_flux_7_vert_strata_log_error_plot.png")
#savefig(error_hist_plot, "7_19_4_0_ticket_density_inj_meas_7_vert_strata_ticket_error_hist_plot.png")
#savefig(Plot_flux_time, "7_19_4_0_ticket_MS_avged_exits_flux_7_vert_strata_inj_meas_flux.png")
#savefig(Plot_flux_weights_time, "7_19_4_0_ticket_MS_avged_exits_flux_weights_7_vert_strata.png")
#savefig(Plot_inj_sizes, "7_19_4_0_ticket_MS_avged_exits_7_vert_inj_meas_sizes.png")