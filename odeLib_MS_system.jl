using LinearAlgebra
using Random
using SciPy

using Parameters

abstract type ODEParams end
#requies
#  h :: Float64
#  f::Function
#  g::Function
# sqrth :: Float64 =sqrt(h);#SQRT Step size for intgrator


@with_kw struct LangevinParams <: ODEParams
    h :: Float64 = 0.001  #Step size for intgrator 
    sigma = [0.5,0.5,0.5]
    alpha::Float64=1.0
    beta::Float64=3.5
    epsilon::Float64=0.5
    f::Function=f_MS_double_well
    g::Function=g_noise_MS
    sqrth :: Float64 =sqrt(h);#SQRT Step size for intgrator
end

#Here, x=(p,q) is a 2-dimensional vector
function f_MS_double_well(x :: Array{Float64,1},t::Float64, p::LangevinParams) :: Array{Float64,1}
    d=copy(x)
    d[1] =  x[1] - (x[1]^3) - (p.beta)*x[1]*(x[2]^2) 
    d[2] = -(1+ (x[1]^2))*x[2]
    return d
end

function g_noise_MS(t :: Float64, p::LangevinParams) :: Float64
    return sqrt(p.epsilon)
end

function RK_Step!(x::Array{Float64,1},h::Float64, p::ODEParams)
    
    K_1 = h * p.f(x, 0.0, p)
    K_2 = h * p.f(x + 0.5 * K_1, 0.0 + 0.5 * h, p)
    K_3 = h * p.f(x + 0.5 * K_2, 0.0 + 0.5 * h, p)
    K_4 = h * p.f(x + K_3, 0 + h, p)
    x .=x.+ (1.0 / 6.0) .* (K_1 .+ 2.0 .* K_2 .+ 2.0 .* K_3 .+ K_4)
    noise = p.g(h, p) * p.sqrth 
    x[1] = x[1] + noise * randn()
    x[2] = x[2] + noise * randn()
end

function f_L96_1d_well(x :: Array{Float64,1}, t :: Float64, eta :: Float64) :: Array{Float64,1}
    d = copy(x)
    n = size(d)
    d[1] = exp(1.0 * eta * t) * (y[2] - y[n-1]) * y[n] - 8 * exp(2.0 * eta * t) * (y[1]^3) #Simplified IF form for double well in the first coordinate
    d[2] = exp(-1.0 * eta * t) * (y[3] - y[n]) * y[1]
    d[n] = exp(-1.0 * eta * t) * (y[1] - y[n-2]) * y[n-1]
    for i in 3:n-1
        d[i] = exp(-1.0 * eta * t) * (y[i+1] - y[i-2]) * y[i-1]
    end
    return d
end

function g_noise_L96_1d_well(t :: Float64, sigma :: Float64, n :: Int, noise_indices :: Int) :: Array{Float64,1}
    noise = zeros(n)
    for i in 1:n
        if i <= noise_indices
            noise[i] = sigma * sqrt(t)
        end
    end
    return noise
end

dim=36
L = zeros((dim,dim))
L[1,1] = 1.
for i in 2:dim
    L[i,i] = -1.
end

function IFRK_step(current_point, ODE_params, f, g, L)
    point_x = current_point.x
    h = ODE_params.h
    eta = ODE_params.eta
    v = copy(point_x)
    K_1 = h * f(v, 0, eta)
    K_2 = h * f(v + 0.5 * K_1, 0.5 * h, eta)
    K_3 = h * f(v + 0.5 * K_2,  0.5 * h, eta)
    K_4 = h * f(v + K_3, 0 + h, eta)
    v = v + (1.0 / 6.0) * (K_1 + 2. * K_2 + 2. * K_3 + K_4)
    for i in 1:size(v)
        point_x[i] = v[i]  * exp(L[i,i] * h)
        point_x[i] = point_x + g(h, ODE_params.sigma, size(point_x), ODE_params.noise_indices) * randn()
    end
    return ODEState(Point_x)
end