using LinearAlgebra
using Random
using SciPy
using Distributions

using Parameters

import Base.+,Base.-


## type defs
abstract type AbstractDistribution end
abstract type AbstractDistributionWeights <: AbstractDistribution end
abstract type AbstractStrata end
abstract type AbstractState end
abstract type ODEParams end

mutable struct ODEState <: AbstractState
    x::Array{Float64,1}
end


#### Paramiter defs

@with_kw struct strataParams
    ode::ODEParams
    innerR=1.0; #Inner redius for Ellipsoids
    outerR=2.0; #Outer redius for Ellipsoids
    useTicket=false; #true then use ticket
   
end

###Types of strata

struct rectangularStrata <:  AbstractStrata # This one don't work yet
    ranges::Array{Array{Float64,1},1}
end

struct ellipticalStrata_Rn <:  AbstractStrata
    center::AbstractState
    Q::Array{Float64,2}
    neighboringStrata::Array{AbstractStrata,1}
    
    function ellipticalStrata_Rn(c::AbstractState,q::Array{Float64,2})
        n=AbstractStrata[];
        new(c,q,n)
    end
end

### types of distributions

mutable struct PointDistibutionWeights  <: AbstractDistributionWeights
    points::Array{AbstractState,1};
    weights::Array{Float64,1};
    normalized::Bool
    function PointDistibutionWeights()
        stck=AbstractState[];
        weights=Float64[];
        new(stck,weights,false);
    end
    function PointDistibutionWeights(p::AbstractState,w::Float64=1.0)
        stck=AbstractState[];
        wghts=Float64[];
        push!(stck,deepcopy(p))
        push!(wghts,w)
        new(stck,wghts,false)
    end
    function PointDistibutionWeights(p::AbstractState)
        PointDistibution(p,1.0)
    end
end

function normalize!(p::PointDistibutionWeights)
    tot=sum(p.weights)
    p.weights=p.weights./tot
    p.normalized=true;
end

mutable struct PointDistibution  <: AbstractDistribution
    points::Array{AbstractState,1};
  
    function PointDistibution()
        stck=AbstractState[];
        new(stck);
    end
    function PointDistibution(p::AbstractState)
        stck=AbstractState[];
        wghts=Float64[];
        push!(stck,deepcopy(p))
         new(stck)
    end
end


#### Local systems evolving in strata

mutable struct LocalSystem
    strata::AbstractStrata
    current::AbstractState
    injectionDist::AbstractDistributionWeights
    exitDist::AbstractDistribution
    strataDist::AbstractDistribution
    exitStrataFlux::Dict{AbstractStrata,Float64}
    ticket::Float64 # this is the rand varialbe in [0,1] which is used to leave strata
    
    function LocalSystem(s::AbstractStrata,p::AbstractState,T::DataType,TWeights::DataType,params::strataParams,tk::Float64=-1.0)
        flux=Dict{AbstractStrata,Float64}()
        new(s,p,TWeights(p,1.0),T(),TWeights(),flux,tk)
    end
    
     function LocalSystem(s::AbstractStrata,
                          p::AbstractState,di::AbstractDistribution,de::AbstractDistribution,ds::AbstractDistribution,tk::Float64=-1.0)
         flux=Dict{AbstractStrata,Float64}()
         new(s,p,di,de,ds,flux,tk)
    end
end


#############################################
# Function defs
#############################################


+(p1::ODEState,p2::ODEState)=p1.x+p2.x;
-(p1::ODEState,p2::ODEState)=p1.x+p2.x;
convert(::Type{Array{Float64,1}}, p::ODEState) = p.x;

### Ellipse intersect detection code

function kEllipsoid(x::Float64, dd::Array{Float64,1}, v::Array{Float64,1},scale::Float64)::Float64
    #objective function for ellipsoids_intersect
    return 1.0-dot(v,(((dd ./ (scale^2)) .* x*(1.0 - x))./(dd.*(1.0 - x).+x)).*v)
end

#Function that normalizes the columns of an eigenvector matrix then multiplies them by the corresponding eienvalues
#Not sure if this is actually needed for ellipsoidsintersect
function evecs_rescale!(evecs)
    for i in 1:size(evecs)[2]
        evecs[:,i] = evecs[:,i] / (norm(evecs[:,i])^2)
    end
  #  return evecs
end

function ellipsoidsIntersect(A, B, a, b,scale::Float64)::Bool
    
    eig_info =eigen(A,B)    
    F = eig_info.vectors
    evecs_rescale!(F)
    v = transpose(F)*(a - b)
    res=SciPy.optimize.minimize_scalar(kEllipsoid, bounds=(0.0, 1.0), args=(eig_info.values, v,scale),method="bounded")
    return (res["fun"] >= 0)
    
end




function strataIntersectOuter(s1::ellipticalStrata_Rn,s2::ellipticalStrata_Rn,params::strataParams)::Bool
    return ellipsoidsIntersect(s1.Q,s2.Q,s1.center.x,s2.center.x,params.outerR);

end

function strataIntersectInner(s1::ellipticalStrata_Rn,s2::ellipticalStrata_Rn,params::strataParams)::Bool
    return ellipsoidsIntersect(s1.Q,s2.Q,s1.center.x,s2.center.x,params.innerR);

end
    
function distance(s1::ODEState,s2::ODEState)::Float64
    return norm(s1.x-s2.x);
end
    
function pojectedDistance(s1::ODEState,s2::ODEState,direction::Array{Float64,1})::Float64    
    return dot(s1.x-s2.x,direction);
end

function strataValue(strat::ellipticalStrata_Rn,state::ODEState,params::strataParams)::Float64
    r=state.x-(strat.center.x)
    return scaleFunction(dot(strat.Q*r,r),params);
end

function exitStrataVals(state::AbstractState,nbd::Array{AbstractStrata,1},params::strataParams)::Array{Float64,1}
    nbdValues=[strataValue(nbdStrata,state,params) for nbdStrata in nbd]
    return nbdValues
end

function exitStrataVals(state::AbstractState,nbd::Array{ellipticalStrata_Rn,1},params::strataParams)::Array{Float64,1}
    nbdValues=[strataValue(nbdStrata,state,params) for nbdStrata in nbd]
    return nbdValues
end

function exitStrataProbs(state::AbstractState,nbd::Array{AbstractStrata,1},params::strataParams)::Array{Float64,1}
    vals=exitStrataVals(state,nbd,params)
    tot=sum(vals)
    probs=vals./tot
    return probs
end
  
function exitStrataProbs(state::AbstractState,nbd::Array{ellipticalStrata_Rn,1},params::strataParams)::Array{Float64,1}
    vals=exitStrataVals(state,nbd,params)
    tot=sum(vals)
    probs=vals./tot
    return probs
end

function exitIndex(state::AbstractState,strataVal::Float64,nbd::Array{AbstractStrata,1},
        ticket::Float64,params::strataParams)::Int
    nbdValues=exitStrataVals(state,nbd,params)
    prepend!(nbdValues,strataVal)
    tot=sum(nbdValues)
    probs=cumsum(nbdValues)./tot
    return  findfirst(probs.>ticket)-1
end

function exitIndex(state::AbstractState,nbd::Array{AbstractStrata,1},
        ticket::Float64,params::strataParams)::Int
    strataVal=strataValue(strata,state,params)
    return exitIndex(state,strataVal,nbd,ticket,params)
    
end



function inStrata(strata::AbstractStrata,state::AbstractState,ticket::Float64,params::strataParams)::Bool   
    d=strataValue(strata,state,params)
    if (ticket==-1.0 || d==0.0)
        return (d>0.0)
    end
    return( exitIndex(state,d,strata.neighboringStrata,ticket,params)==0)
end


function updateLocalSystem!(ls::LocalSystem, ls_list::Array{LocalSystem,1}, params::strataParams)::Bool
    if inStrata(ls.strata,ls.current,ls.ticket,params)
        
        #nbd = ls.strata.neighboringStrata
        #nbdValues=exitStrataVals(ls.current,nbd,params)
        #strataVal = strataValue(ls.strata,ls.current,params)
        #prepend!(nbdValues,strataVal)
        #tot=sum(nbdValues)
        #probs=nbdValues./tot
        #updateDistibution!(ls.strataDist,ls.current,probs[1],params)
        #for k in 1:(size(probs)[1]-1)
        #    val = probs[k+1]
        #    j = findfirst(y->y.strata == nbd[k],ls_list)
        #    if val > 0
        #        updateDistibution!(ls_list[j].strataDist,ls.current,val,params)
        #    end
        #end
        
        updateDistibution!(ls.strataDist,ls.current,1.0,params)
        return false
    else
        updateDistibution!(ls.exitDist,ls.current,params)
        ls.current=sampleDistibution(ls.injectionDist,params)
        updateDistibution!(ls.strataDist,ls.current,1.0,params)
        return true
    end
    # returns true if exited
end

function evolveLocalSystem!(ls::LocalSystem,ls_list::Array{LocalSystem,1},params::strataParams,time::Float64=1.0)::Bool
    t=0.0
    tf=time-params.ode.h
    while t<tf
        RK_Step!(ls.current.x,params.ode.h,params.ode)
        t+=params.ode.h
    end
    RK_Step!(ls.current.x,time-tf,params.ode)
    didExit=updateLocalSystem!(ls,ls_list,params)
    
    return didExit # true if exists strata
end
    

function evolveLocalSystemToExit!(ls::LocalSystem,ls_list::Array{LocalSystem,1},params::strataParams,timeStep::Float64=1.0)::Int
    steps=0
    stillInStrata=true
    while (stillInStrata)
        steps+=1
        stillInStrata=!evolveLocalSystem!(ls,ls_list,params,timeStep)
    end
    return steps
end



function scaleFunction(r::Float64,params::strataParams)::Float64
    if r <= params.innerR
        return 1.0
    elseif r > params.innerR
        return 0.0
    else
        return (r-params.innerR)/(params.outerR-params.innerR)
    end
    
end

    
function updateDistibution!(dist::PointDistibution,x::AbstractState,params::strataParams)
    v=deepcopy(x)
    push!(dist.points,v)
end

function updateDistibution!(dist::PointDistibutionWeights,x::AbstractState,w::Float64,params::strataParams)
    v=deepcopy(x)
    push!(dist.points,v)
    push!(dist.weights,w)
end


function sampleDistibution(dist::PointDistibution,params::strataParams)::AbstractState
    return deepcopy(rand(dist.points));
end

function sampleDistibution(dist::PointDistibutionWeights,params::strataParams)::AbstractState
    if (!dist.normalized)
        normalize!(dist)
    end
    prb=rand();
    cumsumP=cumsum(dist.weights)
   
    return deepcopy(dist.points[findfirst(x->x>prb,cumsumP)]);
end

function registerStrata!(strataList::Array{AbstractStrata,1},s::AbstractStrata,params::strataParams)
    if s in strataList
        return false
    end
   for otherStrata in strataList
        if strataIntersectInner(s,otherStrata,params)
                push!(s.neighboringStrata,otherStrata)
                push!(otherStrata.neighboringStrata,s)
        end
    end
    push!(strataList,s);
    
end


#Finding G and the new weights from the exit distri butions
#Version for splitting an exit point into multiple strata
function find_new_G_weights_eig(ls_list :: Array{LocalSystem,1})
    
    J=size(ls_list)[1]
    G = zeros(J,J)
    #ticket = -1.0
    
    for ls in ls_list
        #Find index of current Stratum
        i = findfirst(y->y ==ls,ls_list)#Ideally not needed to find index

        neighbors_strata = ls.strata.neighboringStrata
        neighbors_ls = filter(y -> y.strata in neighbors_strata, ls_list) #Ideally don't need to keep going between strata and ls
    
        for p in ls.exitDist.points
            exit_nbs = filter(y->strataValue(y.strata,p,params) > 0.0,neighbors_ls)
            if size(exit_nbs)[1] > 0
                probs = exitStrataProbs(p,[y.strata for y in exit_nbs],params)
                
                #Find index of exit stratum and update G accordingly
                k = argmax(probs)
                j = findfirst(y->y == exit_nbs[k],ls_list)
                G[i,j] = G[i,j] + 1.0
                
            end
        end
    end
    
    #Normalize G, find the eigenvector, and re-assign the weights
    for i in 1:J
        G_sum = sum(G[i,:])
        for j in 1:J
            if G_sum > 0
                G[i,j] = G[i,j] / G_sum
            end
        end
    end
    G = Transpose(G)
    
    new_weights = (real.(eigen(G).vectors))[:,J]
    if new_weights[1] < 0
        new_weights = -new_weights
    end
    new_weights = new_weights / sum(new_weights)
    
    return [G, new_weights]
end

function find_new_measures_eig(ls_list :: Array{LocalSystem,1}, new_weights :: Array{Float64,1})
    
    unassignedPoints = PointDistibution()
    new_injection_dists = [PointDistibutionWeights() for ls in ls_list]
    J=size(new_injection_dists)[1]
    #ticket = -1.0

    for ls in ls_list
        #Find index of current Stratum
        i = findfirst(y->y ==ls,ls_list)#Ideally not needed to find index

        neighbors_strata = ls.strata.neighboringStrata
        neighbors_ls = filter(y -> y.strata in neighbors_strata, ls_list) #Ideally don't need to keep going between strata and ls
    
        for p in ls.exitDist.points
            exit_nbs = filter(y->strataValue(y.strata,p,params) > 0.0,neighbors_ls)
            if size(exit_nbs)[1] == 0
                updateDistibution!(unassignedPoints, p, params)
            end
            if size(exit_nbs)[1] > 0
                probs = exitStrataProbs(p,[y.strata for y in exit_nbs],params)
                #Find index of exit stratum
                k = argmax(probs)
                j = findfirst(y->y == exit_nbs[k],ls_list)
                updateDistibution!(new_injection_dists[j], p, new_weights[j], params)
            end
        end
    end

    
    return [new_injection_dists, unassignedPoints]
end

function create_2d_strata_list(center_points, Q_list, ticket, params::strataParams)
    strataList = AbstractStrata[]
    ls_list = LocalSystem[]
    
    for i in 1:size(center_points)[1]
        Q = Q_list[i]
        p1 = center_points[i]
        p11 = deepcopy(p1)
        strata1=ellipticalStrata_Rn(p11,Q)
        registerStrata!(strataList,strata1,params)
        ls1=LocalSystem(strata1,p1,PointDistibution,PointDistibutionWeights,params, ticket)
        push!(ls_list, ls1)
    end
    
    return ls_list
end

function stratified_iterations_rel_exits(ls_list, initial_n_exits, n_iter, n_burn_in, params)
    local_point_dists = Any[]
    local_weights = Any[]
    J = size(ls_list)[1]
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
    end
    
    #Initial iteration, to create the starting points
    n_exits = 0
    while n_exits < initial_n_exits
        n_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:size(ls_current.injectionDist.points)[1]
                evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter >= n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/size(ls_current.injectionDist.points)[1])
                    push!(local_point_dists[i], p)
                end
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            ls_current.injectionDist = new_measures[i]
        end
    end
    
    return [local_point_dists, local_weights]
end

function mix_distributions(d1::PointDistibutionWeights, d2::PointDistibutionWeights,alpha::Float64, max_points::Int64)
    if (!d1.normalized)
        normalize!(d1)
    end
        
    if (!d2.normalized)
        normalize!(d2)
    end
    
    new_dist = PointDistibutionWeights()
    
    for i in 1:size(d1.points)[1]
        updateDistibution!(new_dist, d1.points[i], d1.weights[i] * alpha, params)
    end
    
    for i in 1:size(d2.points)[1]
        updateDistibution!(new_dist, d2.points[i], d2.weights[i] * (1.0-alpha), params)
    end
    
    if size(new_dist.points)[1] > max_points
    #while (size(new_dist.points)[1] > max_points)
        n_points_culled = Int(floor(size(new_dist.points)[1] / 2))
        #n_points_culled = Int(floor(size(new_dist.points)[1] - (max_points / 2)))
        for i in 1:n_points_culled
            deleteat!(new_dist.points,1)
            deleteat!(new_dist.weights,1)
        end
    end
    
    return new_dist
end

function stratified_iterations_rel_exits_memory(ls_list, initial_n_exits, n_iter, n_burn_in, alpha, max_points, params)
    local_point_dists = Any[]
    local_weights = Any[]
    inj_dist_sizes = Any[]
    incoming_dist_sizes = Any[]
    
    J = size(ls_list)[1]
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
        push!(inj_dist_sizes, Any[])
        push!(incoming_dist_sizes, Any[])
    end
    
    #Initial iteration, to create the starting points
    n_exits = 0
    while n_exits < initial_n_exits
        n_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:size(ls_current.injectionDist.points)[1]
            #for k in 1:(Int(floor((size(ls_current.injectionDist.points)[1] / size(ls_current.strata.neighboringStrata)[1]))))
                evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter >= n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/size(ls_current.injectionDist.points)[1])
                    #push!(local_weights[i], new_weights[i]*w * size(ls_current.strata.neighboringStrata)[1]/size(ls_current.injectionDist.points)[1])
                    push!(local_point_dists[i], p)
                end
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            
            push!(incoming_dist_sizes[i], size(new_measures[i].points)[1])
            ls_current.injectionDist = mix_distributions(ls_current.injectionDist,new_measures[i], alpha, max_points)
            push!(inj_dist_sizes[i], size(ls_current.injectionDist.points)[1])
        end
    end
    
    return [local_point_dists, local_weights, inj_dist_sizes, incoming_dist_sizes]
end

function stratified_iterations_rel_exits_memory_q_error(ls_list, initial_n_exits, n_iter, n_burn_in, alpha, max_points, bin_endpoints, hist_true, params)
    local_point_dists = Any[]
    local_weights = Any[]
    hist = zeros(size(bin_endpoints)[1] - 1)
    J = size(ls_list)[1]
    
    error_vals = zeros(n_iter - n_burn_in)
    time_vals = zeros(n_iter - n_burn_in)
    
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
    end
    
    #Initial iteration, to create the starting points
    n_exits = 0
    while n_exits < initial_n_exits
        n_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        steps = 0
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:size(ls_current.injectionDist.points)[1]
                steps += evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter > n_burn_in
            n = num_iter - n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/size(ls_current.injectionDist.points)[1])
                    push!(local_point_dists[i], p)
                end
            end
            
            hist = hist .* (n-1)
            langevin_dist_tot_data = create_total_distribution_2D_langevin(local_point_dists, local_weights, J)
            q_dist_tot = langevin_dist_tot_data[2]
            q_dist_tot_weights = langevin_dist_tot_data[3]
            hist += midpoint_histogram_1D(bin_endpoints, q_dist_tot, q_dist_tot_weights)
            hist = hist ./ n
            
            err = hist_TV_dist_density(hist, hist_true, bin_endpoints)
            error_vals[n] = err
            time_vals[n] = steps
            
            local_point_dists = Any[]
            local_weights = Any[]
            
            for i in 1:J
                push!(local_point_dists, ODEState[])
                push!(local_weights, Float64[])
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            ls_current.injectionDist = mix_distributions(ls_current.injectionDist,new_measures[i], alpha, max_points)
        end
    end
    
    return [hist, time_vals, error_vals]
end

function stratified_iterations_constant_exits(ls_list, n_exits, n_iter, n_burn_in, params)
    local_point_dists = Any[]
    local_weights = Any[]
    J = size(ls_list)[1]
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
    end
    
    #Initial iteration, to create the starting points
    num_exits = 0
    while num_exits < n_exits
        num_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:n_exits
                evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter >= n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/(n_exits))
                    push!(local_point_dists[i], p)
                end
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            ls_current.injectionDist = new_measures[i]
        end
    end
    
    return [local_point_dists, local_weights]
end

function stratified_iterations_constant_exits_memory(ls_list, n_exits, n_iter, n_burn_in, alpha, max_points, params)
    local_point_dists = Any[]
    local_weights = Any[]
    inj_dist_sizes = Any[]
    incoming_dist_sizes = Any[]
    
    J = size(ls_list)[1]
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
        push!(inj_dist_sizes, Any[])
        push!(incoming_dist_sizes, Any[])
    end
    
    #Initial iteration, to create the starting points
    num_exits = 0
    while num_exits < n_exits
        num_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:n_exits
                evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter >= n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/(n_exits))
                    push!(local_point_dists[i], p)
                end
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            
            push!(incoming_dist_sizes[i], size(new_measures[i].points)[1])
            ls_current.injectionDist = mix_distributions(ls_current.injectionDist,new_measures[i], alpha, max_points)
            push!(inj_dist_sizes[i], size(ls_current.injectionDist.points)[1])
        end
    end
    
    return [local_point_dists, local_weights, inj_dist_sizes, incoming_dist_sizes]
end

function stratified_iterations_constant_exits_memory_p_error(ls_list, n_exits, n_iter, n_burn_in, alpha, max_points, bin_endpoints, hist_true, params)
    local_point_dists = Any[]
    local_weights = Any[]
    hist = zeros(size(bin_endpoints)[1] - 1)
    J = size(ls_list)[1]
    
    error_vals = zeros(n_iter - n_burn_in)
    time_vals = zeros(n_iter - n_burn_in)
    
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
    end
    
    #Initial iteration, to create the starting points
    num_exits = 0
    while num_exits < n_exits
        num_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        steps = 0
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:n_exits
                steps += evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        if num_iter > n_burn_in
            n = num_iter - n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/n_exits)
                    push!(local_point_dists[i], p)
                end
            end
            
            hist = hist .* (n-1)
            langevin_dist_tot_data = create_total_distribution_2D_langevin(local_point_dists, local_weights, J)
            p_dist_tot = langevin_dist_tot_data[1]
            p_dist_tot_weights = langevin_dist_tot_data[3]
            hist += midpoint_histogram_1D(bin_endpoints, p_dist_tot, p_dist_tot_weights)
            hist = hist ./ n
            
            err = hist_TV_dist_density(hist, hist_true, bin_endpoints)
            error_vals[n] = err
            time_vals[n] = steps
            
            local_point_dists = Any[]
            local_weights = Any[]
            
            for i in 1:J
                push!(local_point_dists, ODEState[])
                push!(local_weights, Float64[])
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            ls_current.injectionDist = mix_distributions(ls_current.injectionDist,new_measures[i], alpha, max_points)
        end
    end
    
    return [hist, time_vals, error_vals]
end

function stratified_iterations_constant_exits_memory_p_error_inj_flux(ls_list, n_exits, n_iter, n_burn_in, alpha, max_points, bin_endpoints,  bin_endpoints_inj, hist_true, hist_true_occ, weights_true, params)
    local_point_dists = Any[]
    local_weights = Any[]
    hist = zeros(size(bin_endpoints)[1] - 1)
    hist_inj = zeros(size(bin_endpoints_inj)[1] - 1)
    J = size(ls_list)[1]
    p_hist_restricted = Any[]
    q_hist_restricted = Any[]
    for i in 1:J
        push!(p_hist_restricted, zeros(size(bin_endpoints)[1] - 1))
        push!(q_hist_restricted, zeros(size(bin_endpoints)[1] - 1))
    end
    global_weights_avg = zeros(J)
    
    p_hist_true_occ = hist_true_occ[1]
    
    error_vals = zeros(n_iter - n_burn_in)
    err_occ_vals = Any[]
    for i in 1:J
        push!(err_occ_vals, zeros(n_iter - n_burn_in))
    end
    err_weights_vals = zeros(n_iter - n_burn_in)
    time_vals = zeros(n_iter - n_burn_in)
    flux_vals = zeros(n_iter - n_burn_in)
    flux_weights_vals = zeros(n_iter - n_burn_in)
    
    for i in 1:J
        push!(local_point_dists, ODEState[])
        push!(local_weights, Float64[])
    end
    
    #Initial iteration, to create the starting points
    num_exits = 0
    while num_exits < n_exits
        num_exits += 1
        for i in 1:J
            ls_current = ls_list[i]
            evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
        end
    end
    
    new_weights_data = find_new_G_weights_eig(ls_list)
    new_weights = new_weights_data[2]
    new_measures_data = find_new_measures_eig(ls_list, new_weights)
    new_measures = new_measures_data[1]
    
    for i in 1:J
        ls_current = ls_list[i]
        ls_current.exitDist = PointDistibution()
        ls_current.strataDist = PointDistibutionWeights()
        ls_current.injectionDist = new_measures[i]
    end
    
    #Create a histogram of the total injection measure in the X_2 direction
    
    num_iter = 0
    while num_iter < n_iter
        num_iter += 1
        
        #Optional: Print the current iteration number
        println(num_iter)
        
        hist_inj = zeros(size(bin_endpoints_inj)[1] - 1)
        for i in 1:J
            hist_inj += new_weights[i] .* midpoint_histogram_1D(bin_endpoints_inj, [p.x[2] for p in ls_list[i].injectionDist.points], [1.0 for p in ls_list[i].injectionDist.points])
        end
        hist_inj = hist_inj ./ sum(hist_inj)
        weights_inj = new_weights ./ sum(new_weights)
        
        steps = 0
        for i in 1:J
            ls_current = ls_list[i]
            for k in 1:n_exits
                steps += evolveLocalSystemToExit!(ls_current,ls_list,params,params.ode.h)
            end
        end
        
        new_weights_data = find_new_G_weights_eig(ls_list)
        new_weights = new_weights_data[2]
        new_measures_data = find_new_measures_eig(ls_list, new_weights)
        new_measures = new_measures_data[1]
        
        hist_inj_new = zeros(size(bin_endpoints_inj)[1] - 1)
        for i in 1:J
            hist_inj_new += new_weights[i] .* midpoint_histogram_1D(bin_endpoints_inj, [p.x[2] for p in ls_list[i].injectionDist.points], [1.0 for p in ls_list[i].injectionDist.points])
        end
        hist_inj_new = hist_inj_new ./ sum(hist_inj_new)
        weights_inj_new = new_weights ./ sum(new_weights)
        
        if num_iter > n_burn_in
            n = num_iter - n_burn_in
            for i in 1:J
                ls_current = ls_list[i]
                for k in 1:size(ls_current.strataDist.points)[1]
                    p=ls_current.strataDist.points[k]
                    w=ls_current.strataDist.weights[k]
                    push!(local_weights[i], new_weights[i]*w/n_exits)
                    push!(local_point_dists[i], p)
                end
            end
            
            hist = hist .* (n-1)
            langevin_dist_tot_data = create_total_distribution_2D_langevin(local_point_dists, local_weights, J)
            p_dist_tot = langevin_dist_tot_data[1]
            p_dist_tot_weights = langevin_dist_tot_data[3]
            hist += midpoint_histogram_1D(bin_endpoints, p_dist_tot, p_dist_tot_weights)
            hist = hist ./ n
            
            #Create the new average of restricted histograms and weights
            for i in 1:J
                p_hist_restricted[i] = p_hist_restricted[i] .* (n-1)
                p_hist_restricted[i] += midpoint_histogram_1D(bin_endpoints, [p.x[1] for p in local_point_dists[i]], local_weights[i])
                p_hist_restricted[i] = p_hist_restricted[i] ./ n
            end
            global_weights_avg = global_weights_avg .* (n-1)
            global_weights_avg = global_weights_avg + new_weights
            global_weights_avg = global_weights_avg ./ n
            
                
            
            err = hist_TV_dist_density(hist, hist_true, bin_endpoints)
            for i in 1:J
                err_occupation = hist_TV_dist_density(p_hist_restricted[i], p_hist_true_occ[i], bin_endpoints)
                err_occ_vals[i][n] = err_occupation
            end
            err_weights = hist_TV_dist_density(global_weights_avg, weights_true, 1:(size(weights_true)[1]+1))
            err_weights_vals[n] = err_weights
            
            
            flux = hist_TV_dist_density(hist_inj, hist_inj_new, bin_endpoints_inj)
            flux_vals[n] = flux
            flux_weights = hist_TV_dist_density(weights_inj, weights_inj_new, 1:(size(weights_inj)[1]+1))
            flux_weights_vals[n] = flux_weights
            error_vals[n] = err
            time_vals[n] = steps
            
            local_point_dists = Any[]
            local_weights = Any[]
            
            for i in 1:J
                push!(local_point_dists, ODEState[])
                push!(local_weights, Float64[])
            end
        end
        
        for i in 1:J
            ls_current = ls_list[i]
            ls_current.exitDist = PointDistibution()
            ls_current.strataDist = PointDistibutionWeights()
            ls_current.injectionDist = mix_distributions(ls_current.injectionDist,new_measures[i], alpha, max_points)
        end
    end
    
    return [hist, time_vals, error_vals, flux_vals, flux_weights_vals, err_occ_vals, err_weights_vals]
end


function create_total_distribution_2D_langevin(local_point_dists, local_weights, J)
    q_dist_tot = Float64[]
    p_dist_tot = Float64[]
    q_dist_tot_weights = Float64[]
    
    for i in 1:J
        dist_current = local_point_dists[i]
        weights_current = local_weights[i]
        for k in 1:size(local_point_dists[i])[1]
            p = local_point_dists[i][k].x[1]
            w = local_weights[i][k]
            q = local_point_dists[i][k].x[2]
            push!(q_dist_tot, q)
            push!(q_dist_tot_weights, w)
            push!(p_dist_tot, p)
        end
    end
    
    return [p_dist_tot, q_dist_tot, q_dist_tot_weights]
end

#Function that creates a histogram vector from a list of points and weights
function midpoint_histogram_1D(bin_endpoints, q_points, q_weights)
    bin_width = bin_endpoints[2] - bin_endpoints[1]
    n_bins = size(bin_endpoints)[1] - 1
    hist = zeros(n_bins)
    weights_sum = sum(q_weights)
    for i in 1:size(q_points)[1]
        for j in 1:n_bins
            if (bin_endpoints[j] <= q_points[i] < bin_endpoints[j+1])
                hist[j] = hist[j] + (q_weights[i] / weights_sum)
            end
        end
    end
    
    return hist
end

function hist_TV_dist_density(hist_1, hist_2, bin_endpoints)
    bin_width = bin_endpoints[2] - bin_endpoints[1]
    n_bins = size(bin_endpoints)[1] - 1
    TV_Val = 0.0
    for i in 1:n_bins
        TV_Val += (1.0 / 2.0) * abs(hist_1[i] - hist_2[i])
    end
    return TV_Val
end




















